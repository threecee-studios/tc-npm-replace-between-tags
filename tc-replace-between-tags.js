module.exports = function replaceBetweenTags( openTag, closeTag, content, file) {
  'use strict';
  if ( file.indexOf( openTag ) < 0  || file.indexOf( closeTag ) < 0 ) {
    return false;
  }
  var isMultiLine = file.substr( file.indexOf( openTag ), file.indexOf( closeTag ) ).indexOf( '\n' ) > 0;
  var newFile = file.substr(0, file.indexOf( openTag ) + openTag.length);
  if ( isMultiLine ) {
    newFile += '\n';
  }
  newFile += content;
  if ( isMultiLine ) {
    var paddingBeforeClose = file.indexOf( closeTag ) - ((file.substr(0, file.indexOf( closeTag ) ).lastIndexOf('\n')) + 1);
    newFile += file.substr( file.indexOf( closeTag ) - paddingBeforeClose, file.length );
  } else {
    newFile += file.substr( file.indexOf( closeTag ), file.length );
  }
  return newFile;
};